import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './home/header/header.component';
import { ExplicationComponent } from './home/explication/explication.component';
import { ArbreComponent } from './home/arbre/arbre.component';
import { Module1Component } from './home/arbre/module1/module1.component';
import { Module2Component } from './home/arbre/module2/module2.component';
import { Module3Component } from './home/arbre/module3/module3.component';
import { HomePageComponent } from './home/home-page/home-page.component';
import { NavComponent } from './home/header/nav/nav.component';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalSkillsComponent } from './home/modal-skills/modal-skills.component';
import { UserPageComponent } from './home/user-page/user-page.component';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ExplicationComponent,
    ArbreComponent,
    Module1Component,
    Module2Component,
    Module3Component,
    HomePageComponent,
    NavComponent,
    ModalSkillsComponent,
    UserPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ClarityModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule ,
    CommonModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot() ,// ToastrModule added ],
  ],
  providers: [
      HttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
