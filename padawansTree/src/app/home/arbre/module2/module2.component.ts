import { Component, OnInit } from '@angular/core';
import {ArbreService} from "../../../services/arbre.service";
import {Router} from "@angular/router";
@Component({
  selector: 'app-module2',
  templateUrl: './module2.component.html',
  styleUrls: ['./module2.component.scss']
})
export class Module2Component implements OnInit {
  userBySkill;
  skillDescription;
  allModule;
  idModule;
  allSkillMod2: any
  opened = false
  constructor( private arbreService: ArbreService, private router: Router)
{ }

  ngOnInit() {
    this.getIdModule();

  }
  change(id){
    console.dir(id);
    this.opened = true
    this.getBySkillsId(id)
  }

  getIdModule(){
     this.arbreService.getModulesApi().subscribe(res => {
       this.allModule = res;

       this.idModule = this.allModule.message[1].id_module
       this.getSkillModule2(this.idModule)
     })
  }

  getSkillModule2(id){
    this.arbreService.getSkillModule2(id).subscribe((res) => {
      this.allSkillMod2 = res;
      console.dir((this.allSkillMod2))
    })
  }
  getBySkillsId(id){
    this.arbreService.getBySkillsId(id).subscribe((res) => {
      this.skillDescription = res;
      console.dir(this.skillDescription);
    })
    console.dir(id);
  }
  redirect(id){
    this.arbreService.redirect(id);
  }

}
