import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate } from "@angular/animations";
import {ArbreService} from "../../services/arbre.service";

@Component({
  selector: 'app-arbre',
  templateUrl: './arbre.component.html',
  styleUrls: ['./arbre.component.scss'],
  animations: [
    trigger("inGoAnimation3", [
      transition(":enter", [
        style({ transform: "translateX(100%)", opacity: 1 }),
        // animate("350ms", style({ transform: "translateX(0)", opacity: 1 }))
        animate("700ms")
      ])
    ]),
    trigger("inGoAnimation2", [
      transition(":enter", [
        style({ transform: "translateY(100%)", opacity: 1 }),
        // animate("350ms", style({ transform: "translateX(0)", opacity: 1 }))
        animate("700ms")
      ])
    ]),
    trigger("inGoAnimation1", [
      transition(":enter", [
        style({ transform: "translateX(-100%)", opacity: 1 }),
        // animate("350ms", style({ transform: "translateX(0)", opacity: 1 }))
        animate("700ms")
      ])
    ])
  ]

})
export class ArbreComponent implements OnInit {
  allmodule :any
module1: boolean;
module2:boolean;
module3:boolean;
  constructor(private arbreService: ArbreService) {
  }

  ngOnInit() {
   this.getModules();
  }

  showmodul1(){
    this.module1=true;
    this.module2=false;
    this.module3=false;
  }
  showmodul2(){
    this.module1=false;
    this.module2=true;
    this.module3=false;
  }
  showmodul3(){
    this.module1=false;
    this.module2=false;
    this.module3=true;
  }

   getModules(){
      this.arbreService.getModulesApi().subscribe((res) => {
        this.allmodule = res;
      })
   }
}
