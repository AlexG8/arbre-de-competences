import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  basic: boolean = false;
  connect: boolean = false;


  FormRegister: FormGroup = this.fb.group({
    firstname: ['', Validators.compose([Validators.required])],
    lastname: ['', Validators.compose([Validators.required])],
    email: ['', Validators.compose([Validators.required, Validators.email])],
    password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
  });


  FormConnect: FormGroup = this.fb.group({
    email: ['', Validators.compose([Validators.required, Validators.email])],
    password: ['', Validators.compose([Validators.required])],
  });


  constructor(private fb: FormBuilder, private userService: UserService, private toastService: ToastrService , private router: Router){ }

  ngOnInit() {

  }
  Register() {
    this.userService.register(this.FormRegister.value).subscribe(data => {
      this.toastService.success('User creted', '');
      this.basic = false;
    }, (err) =>{
      this.toastService.error("user existe");
    }
    );
  }
  Connect(){
    console.dir(this.FormConnect.value, 'ts');
    this.userService.connect(this.FormConnect.value).subscribe(data => {
          this.connect = false;
        }, (err) =>{
          this.toastService.error("fuck you");
        }
    );
  }

  openModalRegister() {
    this.basic = true;
  }
  openModalConnection(){
    this.connect = true;
  }

}
