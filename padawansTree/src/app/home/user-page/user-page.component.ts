import { Component, OnInit, Input} from '@angular/core';
import { ArbreService } from 'src/app/services/arbre.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {
  userBySkill: any;
  constructor(private arbreService: ArbreService, private router: ActivatedRoute) { }

  ngOnInit() {
    const id = this.router.snapshot.paramMap.get("id");


    this.getUserBySkill(id);
  }

  getUserBySkill(id){
    console.dir(id);
    this.arbreService.getUserBySkill(id).subscribe((res) => {
      this.userBySkill = res;
      console.dir(this.userBySkill);
    })
  }

}
