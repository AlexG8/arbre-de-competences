import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Padawan } from '../model/padawan'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiURL: string = "http://localhost:5000/api/"

  constructor(private http: HttpClient) { }

  register(padawan: Padawan){
    console.log(padawan);
    return this.http.post<Padawan>(`${this.apiURL}padawans/register`, padawan)
  }

  connect(connect: Padawan){
    console.log(connect);
    return this.http.post<Padawan>(`${this.apiURL}padawans/authenticate`, connect)
  }
}

