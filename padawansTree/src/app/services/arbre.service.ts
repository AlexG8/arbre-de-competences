import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class ArbreService {

  skillsIs: number;
  apiURL: string = "http://localhost:5000/api/"
  constructor(private http: HttpClient, private router: Router) { }

  getModulesApi(){
    return this.http.get(`${this.apiURL}modules`)
  }
  getSkillModule2(id){
    return this.http.get(`${this.apiURL}skills/modules/${id}`)
  }
  getBySkillsId(id){
    return this.http.get(`${this.apiURL}skills/${id}`)
  }

  redirect(id){
    this.router.navigate([`/userSkills/${id}`])
    this.skillsIs = id;
    return this.skillsIs;
  }
  getUserBySkill(id){
    return this.http.get(`${this.apiURL}padawans/level/${id}`)
  }

}
