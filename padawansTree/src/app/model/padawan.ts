export interface Padawan {

  firstname: string;

  lastname: string;

  email: string;

  password: string;

}
