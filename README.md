# Despcription du projet

Projet permettant aux éléves de Simplon d'avoir une visualisation sur sa montée en compétences sur les 
différents modules et compétences du référentiel durant la formation avec un esprit communautaire


### Pré requis

Les choses dont vous avez besoin pour utiliser ce projet

```
Avoir un editeur de code
Avoir un terminal
Avoir MySql&NodeJS sur sa machine
```

### Lancer le projet


Voici les 3 étapes nécessaires

```
Dans le dossier API-TreeSkills
npm install pour installer les dépendences requises pour l'application
npm run-start dev pour lancer l'api
```
```
Dans le dossier FRONT-TreeSkills
npm install (pour installer les dépendences requises pour l'application)
ng serve pour lancer le front(affichage)
```

## Conception du Projet

*  Lien vers la maquette : https://www.figma.com/file/O2WP3ZOgXNecHH8yAqj0ov/Untitled?node-id=0%3A1
*  MCD/MPD dans le dossier conception du projet
*  POSTMAN requete dans le dossier conception du projet

## TODO-LIST

DAY 1:
* [x] Réflexion et conception de notre application 
* [x] Création du repo GIT, approfondissement des connaissances de GIT via une interface d'exercices (learngitbranching)
* [x] Recherche d'informations et apprentissage des modèles conceptuelle de données
* [x] Création MCD

DAY 2:
* [x] Creation MPD
* [x] Creation BDD
* [x] Creation Maquette/Wireframe > https://www.figma.com/file/O2WP3ZOgXNecHH8yAqj0ov/Untitled?node-id=0%3A1

DAY 3 & 4:
* [x] Creation de l'API
* [x] Développer des composants back end
* [x] Conecter l'api avec notre DB
* [x] Tester avec postman

DAY 5:
* [x] Développer les composants d'accès aux données
* [x] Intégrer notre maquette
* [ ] Rendre notre app responsive(RIP)

## ENTITIES

Padawans:
1.  ID
2.  FIRSTNAME
2.  LASTNAME
3.  EMAIL
4.  PASSWORD

Padawans_has_skills
1.  ID
2.  id_level
3.  id_skill
4.  id_padawan

Modules:
1.  ID
2.  name_module

Levels:
1.  ID
2.  name_level

Skills:
1.  ID
2.  name_skill


## Built With

* [Angular](https://angular.io/) - Framework front (Javascript)
* [npm](https://www.npmjs.com/) - Gestionnaire de dependance
* [NodeJS](https://nodejs.org/en/) - Le langage back (JS)
* [Express](https://expressjs.com/fr/) - Framework de NodeJS
* [MySQL](https://www.mysql.com/fr/) - BDD

## SCREENSHOTS

