import db from '../../../setup/database';

// Notre query s'occupe d'effectuer la requête sur la base de donneés et de renvoyer au service les datas
const Queries = {
  getAll: (param, successCallback, failureCallback) => {
    let sqlQuery = 'SELECT * FROM `padawans`';

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback('No Padawans.');
      }
    });
  },
  getById: (id, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM padawans WHERE ID=${id}`;

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback('No matching Padawan');
      }
    });
  },
  getByLevelId: (id, successCallback, failureCallback) => {
    let sqlQuery = `SELECT padawans.firstname, padawans.lastname, padawan_has_skills.id_level
    FROM padawans, padawan_has_skills WHERE padawans.id = padawan_has_skills.id_padawan
    AND padawan_has_skills.id_skill = ${id}`;

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback('No matching Padawan');
      }
    });
  },
  authenticate: (user, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM padawans WHERE name="${user.email}" AND password="${user.password}"`;

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows[0]);
      } else {
        return successCallback("Incorrect username or password combinaison");
      }
    });
  },
  getByUserEmail: email => {
    let sqlQuery = `SELECT * FROM padawans WHERE email="${email}"`;

    return new Promise((resolve, reject) => {
      db.query(sqlQuery, (err, rows) => {
        if (err) reject(err);
        resolve(rows[0]);
      });
    });
  },
  getByUsername: email => {
    let sqlQuery = `SELECT * FROM padawans WHERE email="${email}"`;

    return new Promise((resolve, reject) => {
      db.query(sqlQuery, (err, rows) => {
        if (err) reject(err);
        resolve(rows[0]);
      });
    });
  },
  register: async padawans => {
    console.log(padawans, 'query');
    return new Promise((resolve, reject) => {
      console.log(padawans.firstname, padawans.lastname, padawans.email, padawans.hashedPassword);
      let sqlQuery = `INSERT INTO padawans (id, firstname, lastname, email, password) VALUES (NULL,"${padawans.firstname}","${padawans.lastname}", "${padawans.email}", "${padawans.hashedPassword}")`;

      db.query(sqlQuery, (err, res) => {
        if (err) reject(err);
        resolve(res);
      });
    });
  }
};

export default Queries;
