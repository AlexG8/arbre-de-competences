import express from "express";
const router = express.Router();

import UserController from "./controller";
import authorize from "../../../helpers/authorize";

//Private routes, only accessible by admin rights
router.get("/", UserController.getAll);
//router.get("/", authorize("user"), UserController.getAll);
//router.get("/:id", authorize("admin"), UserController.getById);
router.get("/:id", UserController.getById)

/*get all padawans with level*/
router.get("/level/:id", UserController.getByLevelId)

//Public routes
router.post("/authenticate", UserController.authenticate);
router.post("/register", UserController.register);

export default router;
