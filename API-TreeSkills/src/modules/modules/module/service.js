import ModuleQueries from './query';

const ModuleServices = {
    getAll: (req, callback) => {
        ModuleQueries.getAll(
          req,
          response => {
            return callback({ success: true, message: response });
          },
          error => {
            return callback({ success: false, message: error });
          }
        );
      },
      getById: (id, callback) => {
        ModuleQueries.getById(
          id,
          response => {
            return callback({ success: true, message: response });
          },
          error => {
            return callback({ success: false, message: error });
          }
        );
      },
}

export default ModuleServices