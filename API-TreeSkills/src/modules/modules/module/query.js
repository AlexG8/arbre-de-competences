import db from '../../../setup/database';


const Queries = {
    getAll: (param, successCallback, failureCallback) => {
        let sqlQuery = 'SELECT * FROM `modules`';

        db.query(sqlQuery, (err, rows) => {
            if (err) {
                return failureCallback(err);
            }
            if (rows.length > 0) {
                return successCallback(rows);
            } else {
                return successCallback('No Modules.');
            }
        });
    },
    getById: (id, successCallback, failureCallback) => {
        let sqlQuery = `SELECT * FROM modules WHERE id_module=${id}`;

        db.query(sqlQuery, (err, rows) => {
            if (err) {
                return failureCallback(err);
            }
            if (rows.length > 0) {
                return successCallback(rows);
            } else {
                return successCallback('No matching Modules');
            }
        });
    },
}

export default Queries;