import express from "express";
const router = express.Router();

import ModuleController from "./controller";

router.get("/", ModuleController.getAll);
router.get("/:id", ModuleController.getById)


export default router;