import ModuleService from './service';

const ModuleController = {
    getAll: (req, res) => {
        ModuleService.getAll(req, result => {
            //Will be executed once the service is finished
            result.success
                ? res.status(200).send(result)
                : res.status(404).send(result);
        });
    },
    getById: (req, res) => {
        ModuleService.getById(req.params.id, result => {
            //Will be executed once the service is finished
            result.success
                ? res.status(200).send(result)
                : res.status(404).send(result);
        });
    }
}


export default ModuleController;