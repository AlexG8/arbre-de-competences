import db from '../../../setup/database';

// Notre query s'occupe d'effectuer la requête sur la base de donneés et de renvoyer au service les datas
const Queries = {
    getAll: (param, successCallback, failureCallback) => {
        let sqlQuery = 'SELECT * FROM `skills`';

        db.query(sqlQuery, (err, rows) => {
            if (err) {
                return failureCallback(err);
            }
            if (rows.length > 0) {
                return successCallback(rows);
            } else {
                return successCallback('No Skills.');
            }
        });
    },
    getById: (id, successCallback, failureCallback) => {
        let sqlQuery = `SELECT * FROM skills WHERE id_skill=${id}`;

        db.query(sqlQuery, (err, rows) => {
            if (err) {
                return failureCallback(err);
            }
            if (rows.length > 0) {
                return successCallback(rows);
            } else {
                return successCallback('No matching Skills');
            }
        });
    },

    getByModuleId: (id, successCallback, failureCallback) => {
        let sqlQuery = `SELECT * FROM skills WHERE id_module=${id}`;

        db.query(sqlQuery, (err, rows) => {
            if (err) {
                return failureCallback(err);
            }
            if (rows.length > 0) {
                return successCallback(rows);
            } else {
                return successCallback('No matching Skills');
            }
        });
    }

};

export default Queries;
