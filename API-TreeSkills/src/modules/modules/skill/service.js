import SkillQueries from "./query";


const SkillServices = {
    getAll: (req, callback) => {
        SkillQueries.getAll(
            req,
            response => {
                return callback({ success: true, message: response });
            },
            error => {
                return callback({ success: false, message: error });
            }
        );
    },
    getById: (id, callback) => {
        SkillQueries.getById(
            id,
            response => {
                return callback({ success: true, message: response });
            },
            error => {
                return callback({ success: false, message: error });
            }
        );
    },
    getByModuleId: (id, callback) => {
        SkillQueries.getByModuleId(
            id,
            response => {
                return callback({ success: true, message: response });
            },
            error => {
                return callback({ success: false, message: error });
            }
        );
    },
};

export default SkillServices;
