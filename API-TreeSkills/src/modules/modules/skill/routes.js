import express from "express";
const router = express.Router();

import SkillController from "./controller";
router.get("/", SkillController.getAll);
router.get("/:id", SkillController.getById)

/* select all skills from module 1*/
router.get("/modules/:id", SkillController.getByModuleId)

export default router;
