import SkillServices from "./service";


const SkillController = {
    getAll: (req, res) => {
        SkillServices.getAll(req, result => {
            //Will be executed once the service is finished
            result.success
                ? res.status(200).send(result)
                : res.status(404).send(result);
        });
    },
    getById: (req, res) => {
        SkillServices.getById(req.params.id, result => {
            //Will be executed once the service is finished
            result.success
                ? res.status(200).send(result)
                : res.status(404).send(result);
        });
    },
    getByModuleId: (req, res) => {
        SkillServices.getByModuleId(req.params.id, result => {
            //Will be executed once the service is finished
            result.success
                ? res.status(200).send(result)
                : res.status(404).send(result);
        });
    },
};

export default SkillController;
