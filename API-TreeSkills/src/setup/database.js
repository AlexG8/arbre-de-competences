import mysql, { Connection } from "mysql2";
require("dotenv").config();

var connection = mysql.createConnection({
  host:"localhost",
  user: "root",
  password: '',
  database:'tree-skills'
});

//Connecting to database
connection.connect(err => {
  if (err) {
    console.error("error connecting: " + err.stack);
    return;
  }

  console.log("connected DB as id " + connection.threadId);
});

export default connection;
