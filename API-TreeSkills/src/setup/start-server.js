const StartServer = server => {
  console.info("SETUP - Starting server..");

  server.listen(process.env.PORT, error => {
    if (error) {
      console.error("ERROR - Unable to start server.");
    } else {
      console.info(`INFO - Server started on http://localhost:${process.env.PORT} [DEV]`);
    }
  });
};

export default StartServer;
