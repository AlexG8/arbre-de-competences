import express from "express";

import userRoutes from "../modules/modules/user/routes";
import moduleRoutes from '../modules/modules/module/routes';
import skillRoutes from '../modules/modules/skill/routes';

const Router = app => {
  var apiRoutes = express.Router();

  // Home route. We'll end up changing this to our main front end index later.
  app.get("/", function(req, res) {
    res.send("This Route is not yet defined.");
  });

  //Project router
  app.use("/api/", apiRoutes);
  
  app.use("/api/padawans", userRoutes);
  app.use("/api/modules", moduleRoutes);
  app.use("/api/skills", skillRoutes)
};

export default Router;
